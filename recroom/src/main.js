import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = false;

// Styles
require('./styles/css/normalize.css');
require('./styles/css/skeleton.css');

new Vue({
  render: (h) => h(App),
}).$mount('#app');
