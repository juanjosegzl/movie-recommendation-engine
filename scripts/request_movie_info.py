"""
Copyright (c) 2020 Juan José González

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import concurrent.futures
import csv
import os
import requests
import sys
import threading

from utils import get_movie_datastructure


API_BASE_URL = f'https://www.omdbapi.com/?apikey={os.environ["MOVIE_APIKEY"]}&i='
OUTPUT_FOLDER = os.environ.get('OUTPUT_FOLDER', 'json_output')


def get_movie_data(movie):
    """
    Given a movie dictionary request the movie database API to
    fetch movie metadata
    """

    try:
        fname = os.path.join(OUTPUT_FOLDER, movie['id'] + '.json')
        if os.path.isfile(fname):
            print(f"{movie['title']} already saved, omitting", file=sys.stderr)
            return False

        # Request the API
        url = API_BASE_URL + movie['id']
        r = requests.get(API_BASE_URL + movie['id'])

        # Save to the file
        with open(fname, 'w') as fd:
            fd.write(r.text)

    except Exception as e:
        # Catch everything to print the error, otherwise thread will omit it
        print(e)

if __name__ == '__main__':

    # First create arguments for all movies
    arguments = list()
    for filename in sys.argv[1:]:
        movies = get_movie_datastructure(filename)

        for movie in movies:
            arguments.append(movie)

    # Run all the jobs with a pool of workers
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        executor.map(get_movie_data, arguments)
