"""
Copyright (c) 2020 Juan José González

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import concurrent.futures
import csv
import re
import sys
import threading
import wikipedia

from utils import get_movie_datastructure


def load_techniques():
    """
    Load cinematic techniques from an external file, one per line
    """
    with open('techs') as tech_fd:
        techs = [line.strip() for line in tech_fd]
    return techs

# TECHS will be used on threads
TECHS = load_techniques()


def get_page_title(movie):
    """
    Return the (possibly) title of the Wiki page

    Sometimes movies have common words as titles, such as, The lighthouse
    Wikipedia appends (film) of in case of multiple films (YYYY film)
    this function guesses the page title
    """
    movie_title = movie['title']
    movie_year = movie['year']

    all_possible_titles = wikipedia.search(movie_title)
    try:
        if 'film)' in ''.join(all_possible_titles):
            # Disambiguation is needed
            film_results = [title for title in all_possible_titles if '(film)' in title]
            year_film_results = [title for title in all_possible_titles if f'({movie_year} film)' in title]
            if len(year_film_results) > 1:
                print("CASE NOT EXPECTED", file=sys.stderr)
                exit()
            elif len(year_film_results) == 1:
                page_title = year_film_results[0]
            elif len(film_results) == 1:
                page_title = film_results[0]
            else:
                print('Soemthing funny', file=sys.stderr)
                page_title = movie_title
        else:
            page_title = movie_title
    except IndexError:
        print(movie_title, file=sys.stderr)
        print(all_possible_titles, file=sys.stderr)
        # since this is a quick script to fetch data and prove a concept
        # I did not expend too much time thinking in all escenarios
        # in case of this error something funny happened that has to be debugged
        exit()

    return page_title

# Lock for printing, I guess this is not needed at the end (?)
LOCK = threading.Lock()


def parse_wiki_page(movie):
    """
    Given a movie dictionary, (id, title, year) query the wikipage
    look for techniques found in the article and write the results obtained

    meant to be in threads
    """

    try:
        page_title = get_page_title(movie)

        response = ""

        page = wikipedia.page(page_title)

        for tech in TECHS:
            matches = re.findall(re.escape(tech) + r'\b', page.content, re.IGNORECASE)
            if matches:
                response = "\n".join([response, f"\"{movie['id']}\",\"{movie['title']}\"\"{tech}\",\"{len(matches)}\""])

    except wikipedia.exceptions.PageError:
        print("Page not found", file=sys.stderr)
    except wikipedia.exceptions.DisambiguationError as e:
        print(e, file=sys.stderr)
    except Exception as e:
        print(e)

    if response:
        with LOCK:
            # direct printing to stdout, redirect it to a file
            print(response, end="")


if __name__ == '__main__':

    # First create arguments for all movies
    arguments = list()
    for filename in sys.argv[1:]:
        movies = get_movie_datastructure(filename)

        for movie in movies:
            arguments.append(movie)

    # Run all the jobs with a pool of workers
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        executor.map(parse_wiki_page, arguments)
