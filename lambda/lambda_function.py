import json
import boto3
from boto3.dynamodb.conditions import Key

import numpy as np
import pandas as pd
from io import StringIO
import decimal


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)


s3 = boto3.resource('s3')
indices_file = s3.Object('recroom-itesoasn', 'indices.csv')
cosine_file = s3.Object('recroom-itesoasn', 'cosine.csv')

indices_text = indices_file.get()['Body'].read().decode('ISO-8859-1')
indices_buffer = StringIO(indices_text)

indices_ = pd.read_csv(indices_buffer)
indices_ = indices_.reset_index()
indices = pd.Series(indices_.index, index=indices_['imdbID'])

cosine_text = cosine_file.get()['Body'].read().decode('ISO-8859-1')
cosine_buffer = StringIO(cosine_text)
cosine_sim = np.genfromtxt(cosine_buffer, delimiter=",")


# Function that takes in movie title as input and outputs most similar movies
def get_recommendations(imdbID, n=10):
    # Get the index of the movie that matches the title
    idx = indices[imdbID]

    # Get the pairwsie similarity scores of all movies with that movie
    sim_scores = list(enumerate(cosine_sim[idx]))

    # Sort the movies based on the similarity scores
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)

    # Get the scores of the 10 most similar movies
    sim_scores = sim_scores[1:n+1]

    # Get the movie indices
    movie_indices = [i[0] for i in sim_scores]

    # Return the top 10 most similar movies
    return indices.iloc[movie_indices]


dynamodb = boto3.resource('dynamodb')


def get_movie(imdbID):
    table = dynamodb.Table('movies')
    results = table.query(
        KeyConditionExpression=Key('imdbID').eq(imdbID),
        Limit=1,
    )
    return results.get('Items')[0]


def lambda_handler(event, context):
    arg_email = event['queryStringParameters'].get('email')

    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('user_likes')

    results = table.query(
        KeyConditionExpression=Key('user_email').eq(arg_email),
        Limit=10,
    )
    likes = results.get('Items')[0]['likes'][:5]
    print(likes)

    response = {}

    for m in likes:
        recs = get_recommendations(m, n=5)
        response[m] = [get_movie(indices_.iloc[r]['imdbID']) for r in recs]

    return {
        'headers': {
            "Access-Control-Allow-Origin" : "*",
            'Content-Type': 'application/json',
        },
        'statusCode': '200',
        'body': json.dumps(response, cls=DecimalEncoder)
    }
