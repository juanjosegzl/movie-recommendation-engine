import decimal
import json
import boto3
from boto3.dynamodb.conditions import Key, Attr

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

def lambda_handler(event, context):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('user_likes')
    
    print(event)
    
    likes = json.loads(event['queryStringParameters'].get('likes'))
    arg_email = event['queryStringParameters'].get('email')
    
    table.delete_item(
        Key={'user_email': arg_email},
    )
    item = {
        'user_email': arg_email,
        'likes': likes
    }
    table.put_item(
        Item=item
    )
    
    return {
        'headers': {
            "Access-Control-Allow-Origin" : "*",
            'Content-Type': 'application/json',
        },
        'statusCode': '200',
        'body': json.dumps(item, cls=DecimalEncoder)
    }
