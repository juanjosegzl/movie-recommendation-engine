import json
import concurrent.futures
import csv
import os
import urllib3
import threading
import boto3
from io import StringIO
from decimal import Decimal

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('movies')


def get_movie_datastructure(csv_file):
    """
    Convert a CSV line to a dictionary with IMDB id, title and year for movies

    This CSV format is the one used by IMDB lists, it seems users create those
    and IMDB offer them to export them
    """
    csvreader = csv.reader(csv_file)
    movies = [
        {'id': row[1], 'title': row[5], 'year': row[10]}
        for row in csvreader
    ]
    return movies[1:]

API_BASE_URL = f'https://www.omdbapi.com/?apikey={os.environ["MOVIE_APIKEY"]}&i='
OUTPUT_FOLDER = os.environ.get('OUTPUT_FOLDER', 'json_output')

def get_rating(item):
    imdb_rating = list(filter(
        lambda x: x['Source'] == 'Internet Movie Database',
        item.get('Ratings', [])
    ))[0]
    return Decimal(imdb_rating['Value'].split('/')[0])

def get_movie_data(movie):
    """
    Given a movie dictionary request the movie database API to
    fetch movie metadata
    """

    try:
        http = urllib3.PoolManager()
        fname = os.path.join(OUTPUT_FOLDER, movie['id'] + '.json')
        #if os.path.isfile(fname):
        #    print(f"{movie['title']} already saved, omitting", file=sys.stderr)
        #    return False

        # Request the API
        url = API_BASE_URL + movie['id']
        r = http.request('GET', url)

        # Save to the file
        json_item = json.loads(r.data.decode('utf-8'))
        
        json_item['RatingIMDB'] = get_rating(json_item)
        
        table.put_item(
            Item=json_item
        )

    except Exception as e:
        # Catch everything to print the error, otherwise thread will omit it
        print(json_item)
        print(e)


def lambda_handler(event, context):
    files = [record['s3']["object"]["key"] for record in event.get("Records", [])]
    
    s3 = boto3.resource('s3')


    arguments = list()
    for fname in files:
        obj = s3.Object('recroom-itesoasn', fname)
        scsv = obj.get()['Body'].read().decode('ISO-8859-1')
        f = StringIO(scsv)
        movies = get_movie_datastructure(f)

        for movie in movies:
            arguments.append(movie)
    
    # Run all the jobs with a pool of workers
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        executor.map(get_movie_data, arguments)
   
    return {
        'statusCode': 200,
        'body': json.dumps(arguments)
    }
