import decimal
import json
import boto3
from boto3.dynamodb.conditions import Key, Attr

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

def lambda_handler(event, context):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('user_likes')
    
    arg_email = event['queryStringParameters'].get('email')

    results = table.query(
        KeyConditionExpression=Key('user_email').eq(arg_email),
        Limit=1,
    )
    
    return {
        'headers': {
            "Access-Control-Allow-Origin" : "*",
            'Content-Type': 'application/json',
        },
        'statusCode': '200',
        'body': json.dumps(results.get('Items')[0]["likes"], cls=DecimalEncoder)
    }
