import decimal
import json
import boto3
from boto3.dynamodb.conditions import Key, Attr

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

def lambda_handler(event, context):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('movies')

    arg_year = event['queryStringParameters'].get('year', '2000')

    results = table.query(
        IndexName='Year-RatingIMDB-index',
        KeyConditionExpression=Key('Year').eq(arg_year) & Key('RatingIMDB').gt(7),
    )

    return {
        'headers': {
            "Access-Control-Allow-Origin" : "*",
            'Content-Type': 'application/json',
        },
        'statusCode': '200',
        'body': json.dumps(results.get('Items'), cls=DecimalEncoder)
    }
